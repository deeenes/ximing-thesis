
require(ggplot2)
require(reshape2)
require(GGally) # for the ggpairs plot matrix

source('theme_black.R')

data <- read.table('data.tab', sep = '\t', header = TRUE)

data_na.omit <- na.omit(data)
data_noage <- data_na.omit[,-1]

mdata <- melt(data, id = c('Age'))

p <- ggplot(mdata, aes(x = Age, y = value)) +
	geom_point() +
	geom_smooth(method = 'lm') +
	facet_wrap(~ variable) +
	xlab('Age (years)') +
        ylab('Proportion of cells (%)') +
	ggtitle('Proportion of cell types by age') +	
	theme_bw() +
	theme(
		text = element_text(family = 'DINPro')
	)

ggsave('cell_types_by_age.pdf', device = cairo_pdf, width = 7, height = 7)

correlations <- list()

for(i in seq(2, dim(data)[2])){
	print(names(data)[i])
	correlations[[names(data)[i]]] <- cor.test(data$Age, data[,i])

}

# PCA

options(na.action = na.omit)
pca <- prcomp(na.omit(data_noage), center = TRUE, scale = TRUE)

cairo_pdf(filename = 'pca_cell_type.pdf', width = 7, height = 7, family = 'DINPro')
	plot(pca$x[,1:2])
        text(pca$x[,1:2], labels = data_na.omit$Age, pos = 3)
dev.off()

# scatterplot / density plot matrix

regr <- function(data, mapping, ...){
  p <- ggplot(data = data, mapping = mapping) + 
    geom_point() + 
    geom_smooth(method='lm', fill="gray", color="black", lwd = .2, ...)
  p
}


cairo_pdf(filename = 'cell_type_scatterplot_matrix.pdf', width = 9, height = 9, family = 'DINPro')
	ggpairs(data, lower = list(continuous = regr))
dev.off()

# boxplot

p <- ggplot(mdata, aes(x = variable, y = value)) +
	geom_boxplot() +
	xlab('Cell type') +
	ylab('Proportion (%)') +
	ggtitle('Proportions of cell types') +
	theme_bw() +
	theme(
	      text = element_text(family = 'DINPro')
	)

ggsave('cell_types_boxplot.pdf', device = cairo_pdf, width = 3.5, height = 5)
